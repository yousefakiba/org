---
title: Staff 💼
description: Grey Software's Team
category: Team
position: 9
staff:
  - name: Arsala
    avatar: https://gitlab.com/uploads/-/system/user/avatar/2274539/avatar.png
    position: President
    github: https://github.com/ArsalaBangash
    gitlab: https://gitlab.com/ArsalaBangash
    linkedin: https://linkedin.com/in/ArsalaBangash
  - name: Raj
    avatar: https://gitlab.com/uploads/-/system/user/avatar/8089604/avatar.png
    position: Designer
    github: https://github.com/teccUI
    gitlab: https://gitlab.com/teccUI
    linkedin: https://www.linkedin.com/in/raj-paul-368827136/
---

<team-profiles :profiles="staff"></team-profiles>
